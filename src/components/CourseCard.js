import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard(props){
	// console.log(props);
	// console.log(typeof props);

	//object destructuring
	const {name, description, price, _id} = props.courseProp
	//setter getter initial values
// 	const [count,setCount] = useState(0);
// 	const [seatCount,seatSetCount] = useState(10);
// 		console.log(useState(0));
// //activity s49
// 	function enroll(){
// 		if(count<10){
// 		setCount(count + 1);
// 		seatSetCount(seatCount -1 );
// 		console.log(`Enrollees: ${count}`);
// 		}else{
// 			alert("No more seat available, check again later.")
// 		}
// 	}
	return(
		<Card className="cardHighlight p-3">
			<Card.Body>
				<Card.Title className="fw-bold">{name}</Card.Title>

				<Card.Subtitle>Course Description:</Card.Subtitle> 
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="btn-primary" to={`/courseView/${_id}`}>View Details</Link>

			</Card.Body>
		</Card>
	)
};

