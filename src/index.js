import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Peter Parker";
// const element = <h1> Hello, {name}</h1>

// const user = {
//   firstname : 'Levi',
//   lastname  : 'Ackerman'

// }

// const formatName  = (user)=> {
//   return user.firstname + ' ' + user.lastname;
// }

// const fullname = <h1>Hello, {formatName(user)}</h1>


// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(fullname)





