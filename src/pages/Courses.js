import {useState, useEffect} from 'react';
// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'

export default function Courses() {

	const [courses, setCourses] = useState([]);

	useEffect(()=>{
		fetch("http://localhost:4000/courses")
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setCourses(data.map(course => {
				return(
						<CourseCard key={course._id} courseProp = {course}/>
					)
			}));
		});
	},[]);

	// console.log(coursesData);
	// console.log(coursesData[0]);

	// const courses=coursesData.map(course => {
	// 	return(
	// 		<CourseCard key={course.id} courseProp = {course}/>
	// 	)
	// })

	return(
	<>
		<h1>Available Courses:</h1>
		{courses}
	</>
	)
}

