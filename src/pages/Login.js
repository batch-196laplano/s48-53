import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){
//used for user validation
//useContext(provider)
//from app.js
const {user, setUser} = useContext(UserContext);
// console.log(user);

const [email, setEmail]=useState('');
const [password, setPassword]=useState('');
const [isActive, setIsActive]=useState(false);
// console.log(email);
// console.log(password);

	function authenticate(e){
		fetch('http://localhost:4000/users/login',{
			method:'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if (typeof data.accessToken !=="undefined"){
				localStorage.setItem('token', data.accessToken);
				//invoke
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title:"Login Successful",
					icon: "success",
					text: "Welcome to Booking App of 196!"
				})
			}else{
				Swal.fire({
					title:"Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				});
			};
		});

		e.preventDefault();
		//"propertyName/key" , value
		// localStorage.setItem("email", email);

		// setUser({
		// 	email: localStorage.getItem('email')
		// });

		setEmail('');
		setPassword('');

		// alert('Login Successful');
	};

	const retrieveUserDetails = (token) =>{
		fetch('http://localhost:4000/users/getUserDetails', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			});
		})
	}; 

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[email, password]);
		console.log(user);
	return(		
		(user.id !== null)?
			<Navigate to="/courses"/>
		:
		<>
		<h3>Login:</h3>
		<Form onSubmit={e=>authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value= {email}
					onChange= { e => setEmail(e.target.value)}
				/>

			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="password"
					required
					value= {password}
					onChange= { e => setPassword(e.target.value)}
				/>
			</Form.Group>

		<p>Not yet registered? <Link to="/register">Register Here</Link></p>	

		{/*ternary operators { ? : } */}
		{ isActive ?
			<Button className="mt-3 mb-3" variant="success" type="submit" id="submitBtn"> 
				Login
			</Button>
			:
			<Button className="mt-3 mb-3" variant="danger" type="submit" id="submitBtn" disabled> 
				Login
			</Button>
		}
		</Form>
		</>
	)
}