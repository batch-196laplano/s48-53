import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function Register() {
	const history = useNavigate();
	const {user} = useContext(UserContext);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword]=useState('');
	// const [password1, setPassword1]=useState('');
	// const [password2, setPassword2]=useState('');
	const [isActive, setIsActive]=useState(false);
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

		function registerUser(e){
			e.preventDefault();

			fetch('http://localhost:4000/users/checkEmailExists',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email
				})
			})
			.then (res => res.json())
			.then (data => {
				console.log(data)
				if(data){
					Swal.fire({
						title: "Duplicate Email Found",
						icon: "info",
						text: "The email that you are trying to register already exist"
					})
				}else{
					fetch('http://localhost:4000/users',{
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password,
						})
					})
					.then (res => res.json())
					.then (data => {
						console.log(data);
						if (data.email){
							Swal.fire({
								title:'Registration successful',
								icon:'success',
								text:'Something went wrong, try again'
							}) 
							history('/login');
						}
					})
				}
			});
			//clear input fields
			setEmail('');
			setPassword('');
			setFirstName('');
			setLastName('');
			setMobileNo('');

		}


		//hook
		useEffect(()=>{
			//validation on submit button
			// if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			if(email !== '' && password !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length === 11){
				setIsActive(true)
			}else{
				setIsActive(false)
			}
		},[email, password, firstName, lastName, mobileNo]);

		return(
			(user.id !== null)?
				<Navigate to="/courses"/>
			:
			<>
			<h1>Register Here:</h1>
			<Form onSubmit={e=>registerUser(e)}>

				<Form.Group controlId="firstName">
					<Form.Label>First Name:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Input your First Name"
						required
						value= {firstName}
						onChange= { e => setFirstName(e.target.value)}
					/>
				</Form.Group>
				<Form.Group controlId="lastName">
					<Form.Label>Last Name:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Input your Last Name"
						required
						value= {lastName}
						onChange= { e => setLastName(e.target.value)}
					/>
				</Form.Group>
				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control
						type="text"
						placeholder="09**********"
						required
						value= {mobileNo}
						onChange= { e => setMobileNo(e.target.value)}
					/>
				</Form.Group>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						required
						value= {email}
						onChange= { e => setEmail(e.target.value)}
					/>
					<Form.Text>
						We'll never share your email to anyone
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password here"
						required
						value= {password}
						onChange= { e => setPassword(e.target.value)}
					/>
				</Form.Group>

			{/*ternary operators { ? : } */}
			{ isActive ?
				<Button className="mt-3 mb-3" variant="success" type="submit" id="submitBtn"> 
					Register
				</Button>
				:
				<Button className="mt-3 mb-3" variant="danger" type="submit" id="submitBtn" disabled> 
					Register
				</Button>
			}
			</Form>
			</>
		)
}